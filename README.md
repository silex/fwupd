# fwupd for Solus

This repo contains proof of concept packaging files for fwupd.
It is very much a work in progress, expect breakage.

**Note**: use anything from this repo **entirely at your own risk**.
Nothing is properly tested and using fwupd **may break your hardware**.

Simply run `build.sh` to build everything.
The packages can be found in the `packages` directory afterwards.
