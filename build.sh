#!/usr/bin/env bash
set -euo pipefail

DIR="$(realpath $(dirname "$0"))"

patches=(gcab)
deps=(fwupd-efi gcab libjcat libxmlb tpm2-tss)

_clean() {
  rm -vf "$1"/*.eopkg
}

_make_l() {
    pushd "$1"
    make local
    popd
}

_make_mv() {
    pushd "$1"
    make
    sudo rm -vf "/var/lib/solbuild/local/$1-"*.eopkg
    sudo mv ./*.eopkg /var/lib/solbuild/local/
    popd
}

# Patch submodules
for p in "${patches[@]}"; do
  if [ ! -e "${p}/.patched" ]; then
    git -C "${p}" am "${DIR}/patches/${p}/"*.patch
    touch "${p}/.patched"
  fi
done

# Build dependencies
for p in "${deps[@]}"; do
  _clean   "${p}"
  _make_mv "${p}"
done

# build fwupd!
_clean  fwupd
_make_l fwupd

# Move/copy packages to single directory
rm -rf packages
mkdir packages
mv -t packages fwupd/fwupd-1.*-x86_64.eopkg
cp -t packages /var/lib/solbuild/local/{fwupd-efi-1,gcab-1,libjcat-0,libxmlb-0,tpm2-tss-3}*.eopkg

# Create package index
eopkg index -o packages/eopkg-index.xml --skip-signing
